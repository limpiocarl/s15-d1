let myFavFood = 'steak';
console.log(myFavFood);

let sum = 150 + 9;
console.log(sum);

let product = 100 * 90;
console.log(product);

let isActive = true;
console.log(isActive);


let favRestaurants = ['Bar21', 'Bobs', 'Calea', 'Greenhouse', 'Inaka Sushi Bar'];
console.log(favRestaurants);

let favArtist = {
        firstName: 'Justin',
        lastName: 'Vernon',
        stageName: 'Bon Iver',
        birthday: 'April 30,1981',
        age: 40,
        bestAlbum: 'For Emma, Forever Ago',
        bestSong: 'Skinny Love',
        isActive: true
}
console.log(favArtist);

function divide(num1, num2) {
        console.log(num1 / num2);
        return (num1 / num2);
}

divide(40, 5);

let quotient = divide(40, 5);

console.log(`The result of the division is ${quotient}`);

//mathematical operators
let num1 = 5;
let num2 = 10;
let num3 = 4;
let num4 = 40;

// num1 = num1 + num4;
num1 += num4;
console.log(num1);

let string1 = 'Boston';
let string2 = ' Celtics';

// string1 = string1 + string2;
string1 += string2
console.log(string1);

num1 -= string1;
console.log(num1); //NaN

let string3 = 'Hello everyone';
let myArray = string3.split("", 3);
console.log(myArray);


//mathematical operations - follows MDAS/PEMDAS
let mdasResult = 1 + 2 - 3 * 4 / 5;
console.log(mdasResult); //0.6

let pemdasResult = 1 + (2 - 3) * (4 / 5);
console.log(pemdasResult); //0.19

// increment and decrement
// two types increment: pre-fix and post-fix

//pre-fix incrementation
let z = 1;
++z;
console.log(z);

//post-fix incrementation
z++;
console.log(z);

//pre-fix vs post-fix incrementation
console.log(z++);
console.log(z);

console.log(++z);

let n = 1;
console.log(++n); // 1 + n = 2
console.log(n++);
console.log(n); // n + 1 = 3

//pre-fix and post-fix decrementation
console.log(z);
console.log(--z);

//comparison operators - used to compare values
// equality or loose equality operator (==)

console.log(1 == 1); //true
console.log('1' == 1); //true

//strict equality - checks both the value and data type
console.log(1 === 1); //true
console.log('1' === 1); //true

console.log('apple' == 'Apple'); //false

let isSame = 55 == 55;
console.log(isSame);

console.log(0 == false); //forced coercion; true

console.log(0 === false); //false

console.log(1 == true); // true
console.log(true == 'true'); //false

console.log(true == '1'); //true
console.log('0' == false); //true

console.log('Juan' === 'Juan'); //true

//inequality operators (!=)
// checks whether the operand are not equal and/or have different value
//will do type coercion if the operands have different types

console.log('1' != 1); //false because both operands are converted to numbers

console.log('James' != 'John'); //true

console.log(1 != true); //false
//with type conversion: true was converted to 1

//strict inequality (!==)
// it checks whether the two operands have different values and will check if they have different types

console.log('5' !== 5); //true
console.log(5 !== 5); //false

let name1 = 'Juan';
let name2 = 'Betty';
let name3 = 'Peter';
let name4 = 'Pearl';

let number1 = 50;
let number2 = 60;
let numstring1 = '50';
let numstring2 = '60';

console.log(numstring1 == number1);

//relational comparison operators
// a comparison operator - check the relationship between the operands

let x = 500;
let y = 700;
let w = 8000;

let numString3 = '5500';

//greater than (>)
console.log(x > y); //false
console.log(w > y); //true

//less than (<);
console.log(w < y); //false
console.log(y < y); //false
console.log(y > y); //false

console.log(numString3 < 1000); //false
console.log(numString3 < 6000); //true

console.log(numString3 < 'Juan'); //true; logic error; erratic

// logical operators
// and operators (&&) - both operands on the left and right or all operands added must be true or other it is false
// T && T = T
// T && F = F
// F && F = F
// F && T = F

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

let authorization1 = isAdmin && isRegistered;
console.log(authorization1); //false

let requiredLevel = 95;
let requiredAge = 18;

let authorization2 = isRegistered && requiredLevel === 25;
console.log(authorization2);

let authorization3 = isRegistered && isLegalAge && requiredLevel === 95;
console.log(authorization3);

let userName1 = 'gamer2021';
let userName2 = 'shadowMaster'
let userAge1 = 15;
let userAge2 = 30;

let registration = userName1.length > 9 && userAge1 >= requiredAge;
//.length is a property of strings which determine the number of characters
console.log(registration); //false

let registration1 = userName2.length > 8 && userAge2 >= requiredAge;
console.log(registration1); //true

// if
let userName3 = 'crusader_1993';
let userLevel3 = 25;
let userAge3 = 30;

if (userName3.length > 10) {
        console.log("Welcome to the Game Online!");
}

// else statement - will run if the condition given is false or results to false

if (userName3.length <= 10 && userLevel3 <= 25 && userAge3 >= requiredAge) {
        console.log("Thank you for joining Noobies Guild");
} else {
        console.log("You too strong to be noob");
}

// else if executes a statement if the previous or the original condition is false or resulted to false but another specified condition resulted to true

if (userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge) {
        console.log("Thank you for joining Noobies Guild");
} else if (userLevel3 > 25) {
        console.log("You too strong to be noob");
} else if (userAge3 < requiredAge) {
        console.log("You're too young to join the guild");
} else {
        console.log("End");
}

// if-else in function

function addNum(num1, num2) {
        if (typeof num1 === "number" && typeof num2 === "number") {
                console.log("Run only if both arguments passed are number types");
                console.log(num1 + num2);
        } else {
                console.log("One or both of the arguments are not numbers");
        }
}

addNum(2, 5);

// let customerName = prompt("Enter your name");

// if (customerName != null) {
//         document.getElementById("username").value = "Hello" + customerName;
// }

function login(username, password) {
        if (typeof username === "string" && typeof password === "string") {
                console.log("Both arguments are string");
                if (password.length >= 8 && username.length >= 8) {
                        console.log("Welcome");
                }
                else if (username.length < 8) {
                        alert("Username is too short.");
                }
                else if (password.length < 8) {
                        alert("Password is too short.");
                }
        }
        else {
                alert("Credentials are too short.");
        }
}
login("CarlAngelo", "aInZ276768");

// switch statement - used an alternative to an if, else-if, else
/* switch(expression/condition) {
        case value:
                statement:
                break;
        default:
                statement:
                break;
}
 
*/

let hero = "Jose Rizal";

switch (hero) {
        case "Catwoman":
                console.log("Superhero");
                break;
        case "Jose Rizal":
                console.log("National Hero")
                break;
        case "Hercules":
                console.log("Legendary Hero");
                break;
        default:
                console.log("Invalid hero name");
                break;
}

function roleChecker(role) {
        switch (role) {
                case "Admin":
                        console.log("Welcome Admin, to the Dashboard");
                        break;
                case "User":
                        console.log("You are not authorized to view this page");
                        break;
                case "Guest":
                        console.log("Go to the registration page to register");
                        break;
                default:
                        console.log("Invalid Role");
                        break;

        }
}
roleChecker("Guest");

function colorOftheDay(day) {
        if (typeof day === "string") {
                if (day.toLowerCase() === "monday") {
                        alert(`To day is ${day}. Wear black.`)
                } else if (day.toLowerCase() === "tuesday") {
                        alert(`Today is ${day}. Wear green.`)
                } else if (day.toLowerCase() === "wednesday") {
                        alert(`Today is ${day}. Wear yellow.`)
                } else if (day.toLowerCase() === "thursday") {
                        alert(`Today is ${day}. Wear red.`)
                } else if (day.toLowerCase() === "friday") {
                        alert(`Today is ${day}. Wear violet.`)
                } else if (day.toLowerCase() === "saturday") {
                        alert(`Today is ${day}. Wear blue.`)
                } else if (day.toLowerCase() === "sunday") {
                        alert(`Today is ${day}. Wear white.`)
                } else {
                        alert("Invalid input. Enter a valid day of the week")
                }
        }
}
// colorOftheDay("thursday");

function colorOftheDay(day) {
        switch (typeof day === "string" && day.toLowerCase()) {
                case "monday":
                        alert(`Today is ${day}. Wear black.`);
                        break;
                case "tuesday":
                        alert(`Today is ${day}. Wear green.`);
                        break;
                case "wednesday":
                        alert(`Today is ${day}. Wear yellow.`);
                        break;
                case "thursday":
                        alert(`Today is ${day}. Wear red.`);
                        break;
                case "friday":
                        alert(`Today is ${day}. Wear violet.`);
                        break;
                case "saturday":
                        alert(`Today is ${day}. Wear blue.`);
                        break;
                case "sunday":
                        alert(`Today is ${day}. Wear white.`);
                        break;
                default:
                        alert("Invalid input. Enter a valid day of the week");
                        break;
        }
}
// colorOftheDay("Wednesday");

function gradeEvaluator(grade) {
        switch (true) {
                case (grade >= 90):
                        console.log("A");
                        break;
                case (grade >= 80):
                        console.log("B");
                        break;
                case (grade >= 71):
                        console.log("C");
                        break;
                case (grade <= 70):
                        console.log("F");
                        break;
                default:
                        console.log("Invalid");
                        break;
        }
}
gradeEvaluator(95);